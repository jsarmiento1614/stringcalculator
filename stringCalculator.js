// variables globales
var result

const add = (stringToCaculate) => {
  // VALIDAR EL PARAMETRO
  stringToCaculate ? ejecuteVerification(stringToCaculate) : result = 0

  return result
}

function ejecuteVerification (value) {
  // VERIFICO SI TODO EL STRING ES UN NUMERO Y LO RETORNO.
  if (!isNaN(value)) {
    let number = verificationSize(value)

    result = verificationContainNumberNegative([number]) ? `Negativos no soportados -> (${number})` : number
  } else {
    // VERIFICAR SI TRAE SALTO DE LINEA \N
    let newValue
    let intoLine = false
    let delimiter = false

    intoLine = containIntroLine(value)
    intoLine ? newValue = removeIntroLine(value) : intoLine = false

    delimiter = containDelimiter(value)
    delimiter ? newValue = getValueWithDelimiter(value) : delimiter = false
    intoLine === false && delimiter === false ? newValue = value : value = ''

    if (newValue) {
      generateCalc(newValue)
    } else {
      result = newValue
    }
  }
}

function generateCalc (value) {
  let generateSum = 0
  let isNegative = false
  let isNumber
  let traslateInArray = separateString(value, ',')

  for (let i = 0; i < traslateInArray.length; i++) {
    // verifico si es un numero
    isNumber = !isNaN(traslateInArray[i])
    if (isNumber) {
      let sizeNumber = verificationSize(traslateInArray[i])
      let containNegative = verificationContainNumberNegative([ sizeNumber ])

      if (!containNegative) {
        generateSum += sizeNumber
      } else {
        isNegative = true
        break
      }
    }
  }

  isNegative ? result = `Negativos no soportados -> (${value})` : result = generateSum
}

function verificationSize (value) {
  return Number(value) < 1000 ? Number(value) : 0
}

function verificationContainNumberNegative (valueArray) {
  let resp
  let haveNegative = []

  valueArray.forEach(element => {
    element > -1 ? haveNegative.push(false) : haveNegative.push(true)
  })

  for (let i = 0; i < haveNegative.length; i++) {
    haveNegative[i] ? resp = true : resp = false
  }

  return resp
}

function separateString (value, separator) {
  return value.split(separator)
}

function containIntroLine (value) {
  let introLine = false
  let isArray = value.split('\n')

  if (isArray.length > 1) {
    introLine = true
  }

  return introLine
}

function removeIntroLine (value) {
  let stringNumber = ''
  let removedIntroLine
  let arrayOfValue

  removedIntroLine = value.split('\n').join(',')
  arrayOfValue = removedIntroLine.split(',')

  for (let i = 0; i < arrayOfValue.length; i++) {
    // verifico si es un numero
    if (!isNaN(arrayOfValue[i])) {
      stringNumber = stringNumber + ',' + arrayOfValue[i]
    }
  }
  return stringNumber.slice(1)
}

function containDelimiter (value) {
  let resp = false
  let delimiter = value[0] + value[1]

  if (delimiter === '//') {
    resp = true
  }

  return resp
}

function getValueWithDelimiter (value) {
  let intoLine = value.search('\n')
  let delimitator = value.slice(2, intoLine)
  let data = value.slice(intoLine + 1)
  let stringGenerate = data

  // verification multi delimitator
  for (let i = 0; i < delimitator.length; i++) {
    stringGenerate = stringGenerate.split(delimitator[i]).join(',')
  }

  return stringGenerate
}

// console.log(add("10"));
// console.log(add("2,3"));
// console.log(add("2,2000"));
// console.log(add("2,-200"));
// console.log(add("1\n2,3"));
// console.log(add("//;\n1;2"));
// console.log(add("//***\n1***2***3"));
// console.log(add("//*%\n2*2%0"));
// console.log(add("//**%\n1*1%10"));

module.exports = add
