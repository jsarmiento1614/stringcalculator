const calculator = require('../stringCalculator')

test('Espacios en blanco retorna -> 0', () => {
    let send = '';    
    expect(calculator(send)).toBe(0);
});

test('El valor 10 retorna -> 10', () => {
    let send = '10';    
    expect(calculator(send)).toBe(10);
});

test('La suma de 2 + 3 retorna -> 5', () => {
    let send = '2,3';    
    expect(calculator(send)).toBe(5);
});

test('Suma 2 + 2000 retornaria -> 2', () => {
    let send = '2,2000';    
    expect(calculator(send)).toBe(2);
});

test('Suma 2 + (-200) retornaria -> "Negativos no soportados -> (2, -200)"', () => {
    let send = '2,-200';    
    expect(calculator(send)).toEqual('Negativos no soportados -> (2,-200)');
});

test("La cadena 1/n2,3 retornaria -> 6", () => {
    let send = '1\n2,3';    
    expect(calculator(send)).toBe(6);
});

test("La cadena //;/n1;2 retornaria -> 3", () => {
    let send = '//;\n1;2';    
    expect(calculator(send)).toBe(3);
});

test("La cadena //***/n1***2***3 retornaria -> 10", () => {
    let send = '//***\n5***2***3';    
    expect(calculator(send)).toBe(10);
});

test("La cadena //*%/n1*2%5 retornaria -> 8", () => {
    let send = '//*%\n1*2%5';    
    expect(calculator(send)).toBe(8);
});
